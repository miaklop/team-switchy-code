import signac

def main():
    project = signac.init_project('Jigsaw-Signac')
    for kT in [0.1, 0.5, 1.0, 1.5]:
        statepoint = {'kT':kT}
        statepoint["sticky"] = True
        job = project.open_job(statepoint)
        job.init()

if __name__ == '__main__':
    main()
