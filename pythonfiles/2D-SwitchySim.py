
#Importing important things for our code to work correctly!
import hoomd
import hoomd.md
import numpy as np

#Initializing where our code will run
hoomd.context.initialize("");

#Creating a unitcell with one particle as our center. This is our rigid center particle. 
    #The a1,a2, and a3 define our box size.
    #The mass and moment of inertia were found by running init_wrapper on the trapezoid made in mbuild in a different file. 
uc = hoomd.lattice.unitcell(N = 1,
                           a1 = [5, 0, 0],
                           a2 = [0, 5, 0],
                           a3 = [0, 0, 0],
                           dimensions = 2,
                           position = [[0,0,0]],
                           type_name = ['_R0'],
                           mass = [15.0],
                           moment_inertia = [[60.83333333,
                                              812.5,
                                              873.33333333]],
                           orientation = [[1,0,0,0]]);

#This replicates the rigid center particle 5 in the x direction and 5 in the y direction
system = hoomd.init.create_lattice(unitcell=uc, n = [6,6]);

#Adding our two types of particles that we want. 
    #Z is normal particles
    #O is our sticky particles
system.particles.types.add('Z');
system.particles.types.add('O')

#This fills in the positions of our particles to make our trapezoid
    #All positions were found using init_wrapper in another file
particle_positions = [(7.5, -2.16666675,0.0), (-2.5,-2.16666675,0.0), (-12.5,-2.16666675,0.0),
                          (2.5,-2.16666675,0.0), (-7.5,-2.16666675,0.0), (12.5,-2.16666675,0.0),
                          (10.0,0.33333325,0.0), (7.5,2.83333325,0.0), (5.0,0.33333325, 0.0),
                          (0.0,0.33333325, 0.0), (-5.0,0.33333325,0.0), (-10.0,0.33333325,0.0),
                          (2.5,2.83333325, 0.0), (-2.5,2.83333325,0.0), (-7.5,2.83333325,0.0)]
particle_positions = np.array(particle_positions)
#This divides the positions by 10 so they aren't so spread out and look nicer in the simulations
particle_positions /= 7

#Creates the rigid body but nothing is in it yet
rigid = hoomd.md.constrain.rigid();

#Puts the particles in their correct positions based on their types
rigid.set_param('_R0',
               types=['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'O', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'O'],
               #Sets positions of constituent particles
               positions=particle_positions)

#Fills up our rigid body!!
rigid.create_bodies()

#Identifying our neighborlist
nl = hoomd.md.nlist.cell()

#Lennard Jones potential is specified with the desired r_cut value
lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

#Helps not cut the potential off so abruptly
lj.set_params(mode='xplor')

#lj.pair_coeff.set(['O', 'Z', '_R0'], ['O', 'Z', '_R0'], epsilon=1, sigma=1.0, alpha=0)
#lj.pair_coeff.set(['_R0'], ['O', 'Z', '_R0'], epsilon=0, sigma=0)

#Sets the interactions between the different groups of particles. 
lj.pair_coeff.set('O', 'Z', epsilon=1, sigma=0.5, alpha=0)
lj.pair_coeff.set('O', '_R0', epsilon=0, sigma=0)
lj.pair_coeff.set('Z', 'Z', epsilon=1, sigma=0.5, alpha=0)
lj.pair_coeff.set('Z', '_R0', epsilon=0, sigma=0)
lj.pair_coeff.set('_R0', '_R0', epsilon=0, sigma=0, r_cut=False)
lj.pair_coeff.set('O', 'O', epsilon=10.0, sigma=0.8);

#Sets the dt
hoomd.md.integrate.mode_standard(dt=0.005);

#Creates the rigid_center group and applies langevin forces to them
rigid_center = hoomd.group.rigid_center()
integrate = hoomd.md.integrate.langevin(group=rigid_center, kT=0.1, seed=42);

#Writes our results into a .log file
hoomd.analyze.log(filename="runs/Rigid_Trap_Output36NA.log",
                 quantities=['potential_energy',
                            'translational_kinetic_energy',
                            'rotational_kinetic_energy'],
                 period=100,
                 overwrite=True);

#Writes the initial trapezoid into a file that we can see for the poster
hoomd.dump.gsd("runs/Initial_2D_Capsid36NA.gsd", period=None, group=hoomd.group.all(), overwrite=True)

#Writes the simulation into a .gsd file
hoomd.dump.gsd("runs/Rigid_Trap_Trajectory36NA.gsd",
              period=2e3,
              group=hoomd.group.all(),
              overwrite=True);

#Let's run it!
hoomd.run(1e7)
