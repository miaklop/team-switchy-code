import numpy
from matplotlib import pyplot
data = numpy.genfromtxt(fname='fry_runs/36particlerun/Rigid_Trap_Output36.log', skip_header=True);

pyplot.rc('xtick', labelsize=30) 
pyplot.rc('ytick', labelsize=30)
pyplot.rc('axes', labelsize=80)

pyplot.figure(figsize=(20,10), dpi=300);
pyplot.plot(data[2000:,0], data[2000:,1]);
pyplot.xlabel('time step');
pyplot.ylabel('potential_energy');
pyplot.title('Potential Energy vs Time for 36 Particles(Asymmetric)')
pyplot.savefig('PE36.png', transparent=True)

#pyplot.figure(figsize=(20,7), dpi=140);
#pyplot.plot(data[:,0], data[:,2]);
#pyplot.xlabel('time step');
#pyplot.ylabel('temperature');
#pyplot.savefig('Temp36.png')
