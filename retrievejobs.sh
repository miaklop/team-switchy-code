#!/usr/bin/env sh

rsync -rav --ignore-existing --progress rachelsingleton@fry.boisestate.edu:/home/rachelsingleton/team-switchy-code/runs/asymmetric_original_tempchange* fry_runs/temp10-PBC-ordered/asymmetric_original_tempchange
