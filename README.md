# Team-Switchy-Code

## Main Directory Files
* capsid.hoomdxml - trapezoid particle with height of 3 and symmetric sticky patches
    * view by running the command `vmd -hoomd capsid.hoomdxml`
    * ** VMD is needed to run this - I have version 19.3 installed on my computer **
    * created by running Capsid1.ipynb in Jupyter Notebook
* capsid_2D.hoomdxml - copy of capsid.hoomdxml made so that capsid.hoomdxml wouldn't have to change
    * could be created by running Capsid1.ipynb with same dimensions as capsid.hoomdxml
    * The data in this file is used to run 2D-SwitchySim.py
    * ** VMD is needed to run this - I have version 19.3 installed on my computer **
    * ** Note: Box looks bigger even though we say this is "2D". This is not a problem because we just use the data from the file, not the actual file itself. **
    * view by running `vmd -hoomd capsid_2D.hoomdxml`
* matplotlibrc - file that sets up the defaults for using matplotlib
* rigid_info.json - created by running trap_position_data.ipynb before the file was moved to jupyterfiles/
    * contains information about the positions and types for the trapezoid we passed into trap_position_data.ipynb
    * would be rewritten everytime trap_position_data.ipynb was ran if they were still in the same directory
* retrievejobs.sh - file for grabbing jobs off of fry
    * I recommend you make your own - this is set up for a specific directory on my machine

## Python Files
### Anything ending with .py is in this directory!
#### For all simulation files, you can name the output files whatever you would like but make sure to fix the directories they are going to since they will be different on your machine.
#### All simulation files need hoomd-blue and numpy installed to run
* 2D-SwitchySim.py - original simulation code that runs with a symmetric trapezoid (capsid_2d.hoomdxml) 
    * Types and positions came from the .json file created by running trap_position_data.ipynb and then entering them by hand
    * Other data (such as mass) comes from the capsid_2D.hoomdxml file
    * Run with command `python 2D-SwitchySim.py` 
* 2D-SwitchySimPart2.py - runs a simulation for trapezoids with asymmetric sticky patches
    * Temp starts out high to get the particles into random positions and then cools to see how they form triangles
    * Data was copied from capsid_2D.hoomdxml with types changed to make the specific patch types we wanted
    * Run with command `python 2D-SwitchySimPart2.py`
* 2D-SwitchySimPart3.py - similar to Part2 but has only one type of asymmetric patch instead of 6
    * Temp does not change in this one
    * Data was copied from capsid_2D.hoomdxml with types changed to make the specific patch types we wanted
    * Run with command `python 2D-SwitchySimPart3.py`
* JigsawSwitchySim.py - runs simulation with jigsaw trapezoid
    * Jigsaw trapezoid has height 5 versus the 3 that all the others had
    * Postions in the file are read in from rigid_info_jigsaw.json file that is in jupyterfiles/ directory
    * Run with command `python JigsawSwitchySim.py`
* NewTrap-2D-SwitchySim.py - runs a simulation for trapezoids with asymmetric sticky patches but we are using wide trapezoids
    * Code is nearly identical to Part3
    * Run with command `python NewTrap-2d-SwitchySim.py`
* graphingdata.py - creates a potential energy graph
    * Was used for ICUR 2018 poster
    * Need matplotlib and pyplot to run
* original_asymmetric_3.py - runs simulation with original trapezoid but 3 unique patches
    * original trapezoid is one gotten from capsid_2D.hoomdxml
    * each of the three trapezoids has its own type of patch
* wide_asymmetric_1.py - runs simulation with wide trapezoid and only one type of patch
    * wide trapezoid comes from a hoomdxml file in jupyterfiles/ directory
* wide_asymmetric_3.py - runs simulation with wide trapezoid and 3 unique patches
* init.py - initializes a project and a workspace for jigsaw trapezoid
    * need signac installed for it to run
    * only has one statepoint and is for temperature
* init_jigsaw.py - initializes same project but creates more folders in workspace
    * iterates over 4 temperatures and has a statepoint for if the trapezoid has sticky patches or not
    * need signac installed for this to run
* project.py - runs the simulation from init*.py using signac and signac-flow
    * in the process of being set up for init_jigsaw.py
    * needs signac-flow to run
* signac.rc - defaults for signac

## Jupyter Files
### Contains .ipynb files and a few others that are by products of running the code
* capsidwideangle.hoomdxml - trapezoid with height of 3 but base is much wider
    * goal was to see if we could get the trapezoids to fit together better - closer but not perfect
    * view by running `vmd -hoomd capsidwideangle.hoomdxml`
    * created in Capsid1.ipynb
* rigid_info.json - currently contains data for jigsaw trapezoid
    * will be rewritten every time trap_position_data.ipynb is run
* rigid_info_jigsaw.json - contains jigsaw trapezoid data currently
    * will be rewritten when trap_position_data.ipynb is run
    * passed into JigsawSwitchySim.py so that we didn't have to enter the positions by hand
* jigsawcapsid.hoomdxml - trapezoid with height of 5
    * all the particles are the same type - no sticky patches
    * view by running `vmd -hoomd jigsawcapsid.hoomdxml`
    * created from running jigsawtrap.ipynb
* jigsaworiginaltrap.hoomdxml - same as jigsawcapsid.hoomdxml but with different colors
    * each row has a different color
    * was helpful in creating the program that created this trapezoid
    * view by running `vmd -hoomd jigsaworiginaltrap.hoomdxml`
* Rigid_Trap_2D.ipynb - same simulation data that is contained in 2D-SwitchySim.py
    * requires hoomd-blue and numpy be installed on computer and then imported
* init.hoomdxml - is a box full of trapezoids
    * created by running Capsid1.ipynb
    * is rewritten each time code is run
    * is a box full of jigsaw trapezoids right now
    * view by running `vmd -hoomd init.hoomdxml`
* init_jigsaw.hoomdxml
    * created by running jigsawtrap.ipynb
    * rewritten each time code is run
    * view by running `vmd -hoomd init_jigsaw.hoomdxml`
* Poster Lennard Jones.ipynb - has graphs that were used for the ICUR poster
    * needs numpy and matplotlib to run
* Capsid1.ipynb - file where our first trapezoid was made!
    * needs mbuild installed to work
    * allows you to create by hand the trapezoid you want (currently set up for capsid.hoomdxml)
    * creates init.hoomdxml
* jigsawtrap.ipynb - contains code to create the jigsaw trapezoid by using a repeating building block
    * creates jigsawcapsid.hoomdxml when ran
    * creates init_jigsaw.hoomdxml
    * need mbuild and numpy installed and then imported to work
    * had to be fixed to deal with some wrapping issues but since they have been fixed, probably needs to be updated
* trap_position_data.ipynb - file that prints out the correct positions for our trapezoid
    * takes in a .hoomdxml file and creates a .json file that we can then use in our simulation code
    * needs hoomd-blue installed to work
    * need cme_utils repository cloned and then init_wrapper imported to get the data out that we want
    * all of our trapezoids have been run through this code

## Visualization
Since the visualize option in jupyter notebook doesn't work on my mac, I use a docker program to visualize my particles when building them. 

* To download docker: Search on internet. When run, it will download anything it needs to run so you don't have to!
* To run docker: 'docker run -itp 8888:8888 --mount type=bind,source=$(pwd),target=/home/,cmelab/mbuild:latest'
    * I created an alias called mbuild-vis so that I don't have to type it out each time. 
* TIPS:
    * Make sure the docker program is running before you type the command
    * If you can't remember the command and you've created an alias, run `type <alias>`
    * If you run the docker and get the error `docker: Error response from daemon: driver failed programming external connectivity on endpoint elastic_elion (b0c574ede019970602566b773febe57c1c07f21148004b7284750ad24a23cfb6): Bind for 0.0.0.0:8888 failed: port is already allocated`
        it means that you need to fix the ports
    * To do this, (1) use `type <alias>` and copy the command and paste into command line (2) change the 4 digit number on the LEFT to anything above 1000 (I do 9999) (3) run! (4) after you copy the link into your browser, change the 4 digit number to the port number and you're set!

## Files that are on Rachel's Computer (and she doesn't want to forget)
* First Attempt Files:
    * initialconditions.gsd - the trapezoids before running the simulation
        (found from running trap_position_data.ipynb)
    * finaloutput.gsd - final output after letting the simulation run
        (found from running trap_position_data.ipynb)
    * viruscapsid-output.log - data (found from running trap_position_data.ipynb)

* Good Things!:
    * Rigid_trap_Output.log - data from running 2D-SwitchySim.py
    * Rigid_Trap_Trajectory.gsd - vmd file from 2D-SwitchySim.py

* New Files (as of 7/24):
    * Rigid_NewTrap_Output.log - data file NewTrap-2D-SwitchySim.py
    * Rigid_Trap_OutputPart3.log - data file from 2D-SwitchySimPart3.py
    * fry_runs/ - folder with simulation data from running on fry. they are divided up into folders to distinguish
        each simulation run

New Naming Conventions!
    These are for all the files and jobs that I started running after the ICUR conference. Since there was going 
    to be so many of them, I needed a way to distinguish them and this is what I came up with!

    PYTHON FILES:
        <trapezoid type>_<patch position>_<number of unqiue patches>.py
    FRY FOLDERS:
        <particle number>_<patch position>_<trapezoid type>_<number of unique patches>_<runtime exponent>
        Eventually, when we start changing temp, it will be added to the name (OR WE WILL USE SIGNAC)
    OUTPUT FILES:
        Initial_<patch position>_<trapezoid>_<number of unique patches>.gsd
        <trapezoid>_<patch position>_<number of unique patches>_output.log
        <trapezoid>_<patch position>_<number of unique patches>_traj.gsd

