#Importing important things for our code to work correctly!
import hoomd
import hoomd.md
import numpy as np

#Important variables so we don't have to scroll through all the code and change a bunch each time
runTime = 1e7
mix_time = 1e6
particles = 3
epsilon_sticky = 10
sigma_sticky = 0.6
dt = 0.005
kT = 0.1
#Now includes this so that we can heat up the simulation and then cool it back down. 
mix_kT = 50

#Initializing where our code will run
hoomd.context.initialize("");

#Creating a unitcell with three particles as our center. These are our rigid center particles. 
    #The a1,a2, and a3 define our box size.
    #The mass and moment of inertia were found by running init_wrapper on the trapezoid made in mbuild in a different file. 
uc = hoomd.lattice.unitcell(N = particles,
                           a1 = [10, 0, 0],
                           a2 = [0, 18, 0],
                           a3 = [0, 0, 0],
                           dimensions = 2,
                           position = [[0,0,0], [0,6,0], [0,12,0]],
                           type_name = ['_R0', '_R1', '_R2'],
                           mass = [15.0, 15.0, 15.0],
                           moment_inertia = [[60.83333333, 812.5, 873.33333333], [60.83333333, 812.5, 873.33333333], 
                           [60.83333333, 812.5, 873.33333333]],
                           orientation = [[1,0,0,0], [1,0,0,0], [1,0,0,0]]);

#This replicates the rigid center particle 6 in the x direction and 2 in the y direction
system = hoomd.init.create_lattice(unitcell=uc, n = [9,3]);

#Adding our two types of particles that we want. 
    #Z is normal particles
    #A,B,C,D,E,F are our sticky particles
system.particles.types.add('Z')
system.particles.types.add('A')
system.particles.types.add('B')
system.particles.types.add('C')
system.particles.types.add('D')
system.particles.types.add('E')
system.particles.types.add('F')

#This fills in the positions of our particles to make our trapezoid
    #All positions were found using init_wrapper in another file
particle_positions = [(7.5, -2.16666675,0.0), (-2.5,-2.16666675,0.0), (-12.5,-2.16666675,0.0),
                          (2.5,-2.16666675,0.0), (-7.5,-2.16666675,0.0), (12.5,-2.16666675,0.0),
                          (10.0,0.33333325,0.0), (7.5,2.83333325,0.0), (5.0,0.33333325, 0.0),
                          (0.0,0.33333325, 0.0), (-5.0,0.33333325,0.0), (-10.0,0.33333325,0.0),
                          (2.5,2.83333325, 0.0), (-2.5,2.83333325,0.0), (-7.5,2.83333325,0.0)]
particle_positions = np.array(particle_positions)
#This divides the positions by 7 so they aren't so spread out and look nicer in the simulations
particle_positions /= 7

#Creates the rigid body but nothing is in it yet
rigid = hoomd.md.constrain.rigid();

#Puts the particles in their correct positions based on their types
rigid.set_param('_R0',
               types=['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'A', 'Z', 'Z', 'Z', 'F', 'Z', 'Z', 'Z'],
               #Sets positions of constituent particles
               positions=particle_positions)
rigid.set_param('_R1',
               types=['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'B', 'Z', 'Z', 'Z'],
               #Sets positions of constituent particles
               positions=particle_positions)
rigid.set_param('_R2',
               types=['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'E', 'Z', 'Z', 'Z', 'D', 'Z', 'Z', 'Z'],
               #Sets positions of constituent particles
               positions=particle_positions)


#Fills up our rigid body!!
rigid.create_bodies()

#Identifying our neighborlist
nl = hoomd.md.nlist.cell()

#Lennard Jones potential is specified with the desired r_cut value
lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

#Helps not cut the potential off so abruptly
lj.set_params(mode='xplor')

#lj.pair_coeff.set(['O', 'Z', '_R0'], ['O', 'Z', '_R0'], epsilon=1, sigma=1.0, alpha=0)
#lj.pair_coeff.set(['_R0'], ['O', 'Z', '_R0'], epsilon=0, sigma=0)

#Sets the interactions between the different groups of particles.

#Interations dealing with just rigid centers
lj.pair_coeff.set('_R0', '_R0', epsilon=0, sigma=0, r_cut=False)
lj.pair_coeff.set('_R1', '_R1', epsilon=0, sigma=0, r_cut=False)
lj.pair_coeff.set('_R1', '_R2', epsilon=0, sigma=0, r_cut=False)
lj.pair_coeff.set('_R0', '_R1', epsilon=0, sigma=0, r_cut=False)
lj.pair_coeff.set('_R0', '_R2', epsilon=0, sigma=0, r_cut=False)
lj.pair_coeff.set('_R2', '_R2', epsilon=0, sigma=0, r_cut=False)

#Interactions dealing with particle A
lj.pair_coeff.set('A','A', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('A', '_R0', epsilon=0, sigma=0)
lj.pair_coeff.set('A', '_R1', epsilon=0, sigma=0)
lj.pair_coeff.set('A', '_R2', epsilon=0, sigma=0)
lj.pair_coeff.set('A', 'B', epsilon=epsilon_sticky, sigma=sigma_sticky)
lj.pair_coeff.set('A', 'C', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('A', 'D', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('A', 'E', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('A', 'F', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('A', 'Z', epsilon=1, sigma=0.5, alpha=0)

#Interactions dealing with particle B
lj.pair_coeff.set('B','B', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('B', '_R0', epsilon=0, sigma=0)
lj.pair_coeff.set('B', '_R1', epsilon=0, sigma=0)
lj.pair_coeff.set('B', '_R2', epsilon=0, sigma=0)
lj.pair_coeff.set('B', 'C', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('B', 'D', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('B', 'E', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('B', 'F', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('B', 'Z', epsilon=1, sigma=0.5, alpha=0)

#Interactions dealing with particle C
lj.pair_coeff.set('C','C', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('C', '_R0', epsilon=0, sigma=0)
lj.pair_coeff.set('C', '_R1', epsilon=0, sigma=0)
lj.pair_coeff.set('C', '_R2', epsilon=0, sigma=0)
lj.pair_coeff.set('C', 'D', epsilon=epsilon_sticky, sigma=sigma_sticky)
lj.pair_coeff.set('C', 'E', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('C', 'F', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('C', 'Z', epsilon=1, sigma=0.5, alpha=0)

#Interactions dealing with particle D
lj.pair_coeff.set('D','D', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('D', '_R0', epsilon=0, sigma=0)
lj.pair_coeff.set('D', '_R1', epsilon=0, sigma=0)
lj.pair_coeff.set('D', '_R2', epsilon=0, sigma=0)
lj.pair_coeff.set('D', 'E', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('D', 'F', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('D', 'Z', epsilon=1, sigma=0.5, alpha=0)

#Interactions dealing with particle E
lj.pair_coeff.set('E','E', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('E', '_R0', epsilon=0, sigma=0)
lj.pair_coeff.set('E', '_R1', epsilon=0, sigma=0)
lj.pair_coeff.set('E', '_R2', epsilon=0, sigma=0)
lj.pair_coeff.set('E', 'F', epsilon=epsilon_sticky, sigma=sigma_sticky)
lj.pair_coeff.set('E', 'Z', epsilon=1, sigma=0.5, alpha=0)

#Interactions dealing with particle F
lj.pair_coeff.set('F','F', epsilon=0, sigma=0, alpha=0)
lj.pair_coeff.set('F', '_R0', epsilon=0, sigma=0)
lj.pair_coeff.set('F', '_R1', epsilon=0, sigma=0)
lj.pair_coeff.set('F', '_R2', epsilon=0, sigma=0)
lj.pair_coeff.set('F', 'Z', epsilon=1, sigma=0.5, alpha=0)

#Interactions dealing with particle Z
lj.pair_coeff.set('Z','Z', epsilon=1, sigma=0.5, alpha=0)
lj.pair_coeff.set('Z', '_R0', epsilon=0, sigma=0)
lj.pair_coeff.set('Z', '_R1', epsilon=0, sigma=0)
lj.pair_coeff.set('Z', '_R2', epsilon=0, sigma=0)

#Sets the dt
hoomd.md.integrate.mode_standard(dt=dt);

#Creates the rigid_center group and applies langevin forces to them
rigid_center = hoomd.group.rigid_center()
integrate = hoomd.md.integrate.langevin(group=rigid_center, kT=mix_kT, seed=42);

#Writes our results into a .log file
hoomd.analyze.log(filename="runs/asymmetric_original_tempchange/original_asymmetric_6_output.log",
                 quantities=['potential_energy',
                            'translational_kinetic_energy',
                            'rotational_kinetic_energy'],
                 period=100,
                 overwrite=True);

#Writes the initial trapezoid into a file that we can see for the poster
hoomd.dump.gsd("runs/asymmetric_original_tempchange/Initial_asymmetric_original_6.gsd", period=None, group=hoomd.group.all(), overwrite=True)

#Writes the simulation into a .gsd file
hoomd.dump.gsd("runs/asymmetric_original_tempchange/original_asymmetric_6_traj.gsd",
              period=2e3,
              group=hoomd.group.all(),
              overwrite=True);

#Let's run it!
hoomd.run(mix_time)
integrate.set_params(kT=kT)
hoomd.run(runTime)
