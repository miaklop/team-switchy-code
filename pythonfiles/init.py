import signac

def main():
    project = signac.init_project('Jigsaw-Signac')
    for kT in [0.1]:
        statepoint = {'kT':kT}
        job = project.open_job(statepoint)
        job.init()

if __name__ == '__main__':
    main()
